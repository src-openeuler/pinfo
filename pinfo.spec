Name:          pinfo
Version:       0.6.13
Release:       1
Summary:       An user-friendly, console-based viewer for Info documents
License:       GPLv2
URL:           https://github.com/baszoetekouw/pinfo
Source0:       %{url}/archive/v%{version}/%{name}-%{version}.tar.gz
Patch0001:     pinfo-0.6.9-xdg.patch
# https://github.com/baszoetekouw/pinfo/commit/fc67ceacd81f0c74fcab85447c23a532ae482827
# https://github.com/baszoetekouw/pinfo/commit/ab604fdb67296dad27f3a25f3c9aabdd2fb8c3fa
Patch0002:     pinfo-0.6.13-gccwarn.patch
Patch0003:     pinfo-0.6.9-nogroup.patch
# https://github.com/baszoetekouw/pinfo/commit/23c169877fda839f0634b2d193eaf26de290f141
Patch0004:     pinfo-0.6.13-stringop-overflow.patch
Patch0005:     pinfo-0.6.9-infopath.patch
Patch0006:     pinfo-0.6.10-man.patch
Patch0007:     bugfix-of-gcc-10.patch

BuildRequires: ncurses-devel automake gettext-devel libtool texinfo
Requires:      xdg-utils

%description
Pinfo is a hypertext info file viewer with a user interface similar to lynx.
It is based on curses/ncurses, and can handle info pages as well as man pages.
It also features regexp searching and user-defined colors/keys.

%package help
Summary:       Help documents for pinfo
Requires:      %{name} = %{version}-%{release}

%description help
Pinfo-help provides man pages and other related help documents for pinfo.

%prep
%autosetup -n %{name}-%{version} -p1

%build
./autogen.sh
%configure --without-readline
%make_build

%install
%make_install

rm -f $RPM_BUILD_ROOT%{_infodir}/dir

%find_lang %{name}

%files -f %{name}.lang
%license COPYING
%doc AUTHORS
%{_bindir}/pinfo
%{_infodir}/pinfo.info*
%config(noreplace) %{_sysconfdir}/pinforc

%files help
%doc Changelog* NEWS README.md TECHSTUFF
%{_mandir}/man1/pinfo.1*

%changelog
* Sun Oct 08 2023 yaoxin <yao_xin001@hoperun.com> - 0.6.13-1
- Upgrade to 0.6.13

* Tue Aug 3 2021 Shenmei Tu <tushenmei@huawei.com> - 0.6.10-24
- bugfix-of-gcc-10.patch

* Fri July 30 2021 Shenmei Tu <tushenmei@huawei.com> - 0.6.10-23
- bug fix of multiple definition

* Fri Feb 14 2020 lingsheng <lingsheng@huawei.com> - 0.6.10-22
- Package init
